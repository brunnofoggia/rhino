<?php

namespace doctrine;

/**
 * Utility to read the database structure
 *
 * @category    Utilities
 * @author      Bruno Foggia
 * @link        https://bitbucket.org/brunnofoggia/rhino
 */
trait Rhino {
    
    /**
     * DBAL SchemaManager Object
     */
    protected $sm;

    /**
     * Allows connection to be overwritten
     */
    public function setConn($conn) {
        try {
            $this->db = $conn;
            $this->sm = @$this->db->getSchemaManager();
        } catch (\Doctrine\DBAL\Exception\ConnectionException $e) {
            $this->db = null;
            $this->sm = null;
        }

        return ($this->sm !== null);
    }

    /**
     * Get DBAL SchemaManager
     */
    public function getSm() {
        try {
            $this->sm = @$this->db->getSchemaManager();
        } catch (\Doctrine\DBAL\Exception\ConnectionException $e) {
            $this->db = null;
            $this->sm = null;
        }
        
        return $this->sm;
    }

    /**
     * Get All Table Data (name, columns, relations and indexes)
     * @param string $table name of the related table
     * @return array
     */
    public function getTableData($table) {
        $this->getSm();
        $columns = $this->sm->listTableColumns($table);
        $belongsTo = $this->discoverBelongsTo($table);
        $hasMany = $this->discoverHasMany($table);

        $indexes = $this->sm->listTableIndexes($table);

        return [
            'table' => $table,
            'columns' => $columns,
            'belongsTo' => $belongsTo,
            'hasMany' => $hasMany,
            'indexes' => $indexes
        ];
    }

    /**
     * Get BelongsTo relations
     * @param string $tableFkName name of the related table
     * @return array
     */
    public function discoverBelongsTo($tableFkName) {
        $this->getSm();
        $belongsTo = $this->sm->listTableForeignKeys($tableFkName);

        foreach ($belongsTo as $x => $foreignKey) {
            $belongsTo[$x] = [
                'foreignTableName' => $foreignKey->getForeignTableName(),
                'columns' => $foreignKey->getColumns()
            ];
        }

        return $belongsTo;
    }

    /**
     * Get HasMany relations
     * @param string $tableFkName name of the related table
     * @return array
     */
    public function discoverHasMany($tableFkName) {
        $this->getSm();
        try {
            $tables = $this->sm->listTables();

            $hasMany = [];
            foreach ($tables as $table) {
                $tableName = $table->getName();

                $belongsTo = $this->sm->listTableForeignKeys($tableName);
                foreach ($belongsTo as $x => $foreignKey) {
                    if ($foreignKey->getForeignTableName() === $tableFkName) {
                        $hasMany[] = [
                            'foreignTableName' => $tableName,
                            'columns' => $foreignKey->getColumns()
                        ];
                    }
                }
            }
        } catch (Exception $e) {
            throw new Exception($e);
        }

        return $hasMany;
    }

}
