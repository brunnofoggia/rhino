# Rhino

[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/license-MIT-4C9182.svg)](https://opensource.org/licenses/MIT)

Utility to read the database structure

## usage

1. Implement it into your class
 
        class Test {
            use \doctrine\Rhino;
        }

2. Connect to DBAL and set your connection

        $this->setConn($connection);

3. Get All Table Data (name, columns, relations and indexes) calling

        $this->getTableData('table_name');
        
   An example of the result set would be
   
           array (
          'table' => 'test',
          'columns' => 
          array (
            'id' => 
            Doctrine\DBAL\Schema\Column::__set_state(array(
               '_type' => 
              Doctrine\DBAL\Types\IntegerType::__set_state(array(
              )),
               '_length' => NULL,
               '_precision' => 10,
               '_scale' => 0,
               '_unsigned' => false,
               '_fixed' => false,
               '_notnull' => true,
               '_default' => NULL,
               '_autoincrement' => true,
               '_platformOptions' => 
              array (
              ),
               '_columnDefinition' => NULL,
               '_comment' => NULL,
               '_customSchemaOptions' => 
              array (
              ),
               '_name' => 'id',
               '_namespace' => NULL,
               '_quoted' => false,
            )),
            'name' => 
            Doctrine\DBAL\Schema\Column::__set_state(array(
               '_type' => 
              Doctrine\DBAL\Types\StringType::__set_state(array(
              )),
               '_length' => 50,
               '_precision' => 10,
               '_scale' => 0,
               '_unsigned' => false,
               '_fixed' => false,
               '_notnull' => true,
               '_default' => NULL,
               '_autoincrement' => false,
               '_platformOptions' => 
              array (
                'collation' => 'latin1_swedish_ci',
              ),
               '_columnDefinition' => NULL,
               '_comment' => NULL,
               '_customSchemaOptions' => 
              array (
              ),
               '_name' => 'nome',
               '_namespace' => NULL,
               '_quoted' => false,
            )),
          ),
          'belongsTo' => 
          array (
          ),
          'hasMany' => 
          array (
            0 => 
            array (
              'foreignTableName' => 'test_many',
              'columns' => 
              array (
                0 => 'test_id',
              ),
            ),
          ),
          'indexes' => 
          array (
            'primary' => 
            Doctrine\DBAL\Schema\Index::__set_state(array(
               '_columns' => 
              array (
                'id' => 
                Doctrine\DBAL\Schema\Identifier::__set_state(array(
                   '_name' => 'id',
                   '_namespace' => NULL,
                   '_quoted' => false,
                )),
              ),
               '_isUnique' => true,
               '_isPrimary' => true,
               '_flags' => 
              array (
              ),
               'options' => 
              array (
              ),
               '_name' => 'PRIMARY',
               '_namespace' => NULL,
               '_quoted' => false,
            )),
          ),
        )

   Optionally get only what want by using one of the listed bellow

        $columns = $this->getSm()->listTableColumns($table); // list of collumns
        $indexes = $this->getSm()->listTableIndexes($table); // list of indexes
        $belongsTo = $this->discoverBelongsTo($table); // list of belongsTo relations
        $hasMany = $this->discoverHasMany($table); // list of hasMany relations